package ru.zolov.tm.controller;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

@RestController
@RequestMapping(value = "/projects")
public class ProjectController {

  @Autowired ProjectService projectService;
  @Autowired TaskService taskService;

  @GetMapping public List<Project> getProjects() {
    @NotNull List<Project> projects = projectService.findAll();
    return projects;
  }

  @GetMapping(value = "{id}") public Project viewProject(@PathVariable(name = "id") String id) {
    @NotNull final Project project = projectService.findById(id);
    return project;
  }

  @PostMapping public RedirectView createProject(
      @RequestBody final Project project
  ) {
    projectService.create(project);
    return new RedirectView("/projects");
  }

  @PutMapping public RedirectView editProject(@RequestBody final Project project
  ) {
    projectService.update(project.getId(), project.getName(), project.getDescription());
    return new RedirectView("/projects");
  }

  @DeleteMapping public RedirectView deleteProject(@PathVariable(name = "id") String id) {
    taskService.removeAllByProjectId(id);
    projectService.removeById(id);
    return new RedirectView("/projects");
  }
}
