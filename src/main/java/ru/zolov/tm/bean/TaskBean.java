package ru.zolov.tm.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import ru.zolov.tm.entity.Task;

@Named
@ManagedBean
@ViewScoped
public class TaskBean extends AbstractBean {

  public Task create(
      final String projectId,
      final String name
  ) {
    return taskService.create(projectId, name);
  }

  public List<Task> findTaskByProjectId(final String projectId) {
    return taskService.findTaskByProjectId(projectId);
  }

  public void update(final String id, final String name, final String description) {
    taskService.update(id, name, description);
  }

  public void removeTaskById(final String id) {
    taskService.removeTaskById(id);
  }

}
