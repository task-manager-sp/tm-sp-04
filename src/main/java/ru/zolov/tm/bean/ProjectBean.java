package ru.zolov.tm.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;


@Getter
@Setter
@Named
@ManagedBean
@ViewScoped
public class ProjectBean extends AbstractBean {

  @Nullable private String id;
  @Nullable private String name;
  @Nullable private String description;
  @Nullable private String part;
  @Nullable private String dateOfCreate;
  @Nullable private String dateOfStart;
  @Nullable private String dateOfFinish;
  @Nullable private String status;
  @Nullable private String currentProject;


  public Project create(
      final String name,
      final String description
  ) {
    Project project = new Project();
    project.setName(name);
    project.setDescription(description);
    return projectService.create(project);
  }

  public List<Project> findAll() {
    return projectService.findAll();
  }

  public void update(
      final String id,
      final String name,
      final String description
  ) {
    projectService.update(id, name, description);
  }

  public Project findById(final String id) {
    Project project = projectService.findById(id);
    return project;
  }

  public List<Project> findProjectByPart(final String part) {
    return projectService.findProject(part);
  }

  private void eraseBeanState() {
    id = null;
    name = null;
    description = null;
    part = null;
    dateOfCreate = null;
    dateOfStart = null;
    dateOfFinish = null;
    status = null;
    currentProject = null;
  }
}
