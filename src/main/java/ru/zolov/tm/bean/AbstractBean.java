package ru.zolov.tm.bean;

import java.text.SimpleDateFormat;
import javax.inject.Inject;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

public class AbstractBean {

  SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyy");
  @Inject
  ProjectService projectService = new ProjectService();
  @Inject
  TaskService taskService = new TaskService();
}
