package ru.zolov.tm.service;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Task;

@Service
@Transactional
public class TaskService extends AbstractService {

  @Autowired private ITaskRepository taskRepository;

  @SneakyThrows public @NotNull Task create(
      @Nullable final String projectId,
      @Nullable final String name
  ) {
    if (projectId == null || projectId.isEmpty()) throw new Exception();
    if (name == null || name.isEmpty()) throw new Exception();
    Task task = new Task();
    task.setProjectId(projectId);
    task.setName(name);
    taskRepository.save(task);
    return task;
  }

  public @NotNull List<Task> findAllTask() {
    @NotNull List<Task> list = (List<Task>)taskRepository.findAll();
    return list;
  }

  @SneakyThrows public List<Task> findTaskByProjectId(
      @Nullable final String id
  ) {
    if (id == null || id.isEmpty()) throw new Exception();
    @NotNull List<Task> list = new ArrayList<>();
    list = (List<Task>)taskRepository.findAllTasksByProjectId(id);
    return list;
  }

  @SneakyThrows public @Nullable Task findTaskById(
      @Nullable final String id
  ) {
    if (id == null || id.isEmpty()) throw new Exception();
    Task task = taskRepository.findOneById(id).orElseThrow(Exception::new);
    return task;
  }

  @SneakyThrows public void removeTaskById(
      @Nullable final String id
  ) {
    if (id == null || id.isEmpty()) throw new Exception();
    Task task = taskRepository.findOneById(id).orElseThrow(Exception::new);
    taskRepository.delete(task);
  }

  @SneakyThrows public void update(
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description
  ) {
    if (id == null || id.isEmpty()) throw new Exception();
    if (name == null || name.isEmpty()) throw new Exception();
    @NotNull final Task task = taskRepository.findOneById(id).orElseThrow(Exception::new);
    task.setId(id);
    task.setName(name);
    if (description == null || description.isEmpty()) task.setDescription("empty");
    else task.setDescription(description);
    taskRepository.save(task);
  }

  @SneakyThrows public void removeAllByProjectId(@NotNull final String projectId) {
    if (projectId == null || projectId.isEmpty()) throw new Exception("Empty projectID");
    taskRepository.deleteAllByProjectId(projectId);
  }

}
